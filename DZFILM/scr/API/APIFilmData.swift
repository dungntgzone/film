//
//  APIFilmData.swift
//  DZFILM
//
//  Created by Nguyen Tien Dung on 9/17/15.
//  Copyright © 2015 Nguyen Tien Dung. All rights reserved.
//

import UIKit

class APIFilmData: NSObject {
    
    class func HomePageData(success:(data : Dictionary<String,AnyObject>) -> Void,failure:(erMessage : String) -> Void){
        
        PFCloud.callFunctionInBackground("homePage", withParameters: nil) { (data, er) -> Void in
            if(er != nil){
                failure(erMessage: er!.localizedDescription)
            }
            else{
                if(data!.objectForKey("status")!.integerValue == 1){
                    if let listBanner = data![Constants.API.Codes.dashboard_banner] as? [PFObject]{
                        AppShareModel.sharedInstance().listPanner = listBanner
                    }
                    if let homes = data![Constants.API.Codes.dashboard_home] as? [Dictionary<String,AnyObject>]{
                        AppShareModel.sharedInstance().listHome = homes
                    }
                    success(data: data as! Dictionary<String,AnyObject>)
                }
                else{
                    failure(erMessage: data!.objectForKey("message") as! String)
                }
                
            }
        }
    }
    class func GetDataTable(tableName : String,count : Int,skip : Int,success:(data : Array<PFObject>) -> Void,failure:(erMessage : String) -> Void){
        let query = PFQuery(className: tableName)
        query.skip = skip
        query.limit = count
        query.addDescendingOrder("updatedAt")
        query.findObjectsInBackgroundWithBlock { (list : Array<PFObject>?, er : NSError?) -> Void in
            if(list != nil){
                success(data: list!)
            }
            else if (er != nil){
                failure(erMessage: er!.localizedDescription)
            }
            else{
                failure(erMessage: "Lỗi không lấy được dữ liệu")
            }
        }
    }
    class func SearchFilm(count : Int,skip : Int,searchtext : String,success:(data : Array<PFObject>) -> Void,failure:(erMessage : String) -> Void){
        let query = PFQuery(className: "Film")
        query.skip = skip
        query.limit = count
        query.addDescendingOrder("updatedAt")
        query.whereKey("metadata", containsString: searchtext.normalizeVietnameseString())
        
        query.findObjectsInBackgroundWithBlock { (list : Array<PFObject>?, er : NSError?) -> Void in
            if(list != nil){
                success(data: list!)
            }
            else if (er != nil){
                failure(erMessage: er!.localizedDescription)
            }
            else{
                failure(erMessage: "Lỗi không lấy được dữ liệu")
            }
        }
    }
    class func GetFillDetail(filmUUID : String,success:(data : PFObject) -> Void,failure:(erMessage : String) -> Void){
        let query = PFQuery(className: "Film")
        query.whereKey("uuid", containsString: filmUUID)
        query.getFirstObjectInBackgroundWithBlock { (filmDetail : PFObject?, error : NSError?) -> Void in
            if(error != nil){
                failure(erMessage: "Lỗi Máy chủ vui lòng thử lại trong giây lát")
            }
            else{
                success(data: filmDetail!)
            }
        }
    }
}
