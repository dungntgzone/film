//
//  AppShareModel.swift
//  DZFILM
//
//  Created by Nguyen Tien Dung on 9/21/15.
//  Copyright © 2015 Nguyen Tien Dung. All rights reserved.
//

import UIKit

class AppShareModel: NSObject {
    static var instance: AppShareModel?
    var listPanner : [PFObject]!
    var listHome   : [Dictionary<String,AnyObject>]!
    
    class func sharedInstance() -> AppShareModel {
        if !(instance != nil) {
            instance = AppShareModel()
            
        }
        return instance!
    }
}
