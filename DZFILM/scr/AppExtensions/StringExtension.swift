//
//  StringExtension.swift
//  DZFILM
//
//  Created by hien.tt's on 9/25/15.
//  Copyright © 2015 Nguyen Tien Dung. All rights reserved.
//

import Foundation
extension String{

    func normalizeVietnameseString() -> String{
        let originStr = NSMutableString(string: self)
        CFStringNormalize(originStr,CFStringNormalizationForm.D)
        CFStringFold(originStr,CFStringCompareFlags.CompareDiacriticInsensitive,nil)
        let finalString1 = originStr.stringByReplacingOccurrencesOfString("u0111", withString: "d")
        let finalString2 = finalString1.stringByReplacingOccurrencesOfString("u0110", withString: "D")
        return finalString2
    }
}