//
//  Constants.swift
//  DZFILM
//
//  Created by Nguyen Tien Dung on 9/18/15.
//  Copyright © 2015 Nguyen Tien Dung. All rights reserved.
//

import CoreGraphics
import Foundation
import UIKit
struct Constants {
    struct System {
        static let scare : CGFloat = UIScreen.mainScreen().bounds.size.width/320.0
        static let screenSize : CGSize = UIScreen.mainScreen().bounds.size
        static let delegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        static let deviceVersion = (UIDevice.currentDevice().systemVersion as NSString).floatValue
        static let baseAppColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }
    struct API {
        struct Codes {
            static let dashboard_banner = "banner"
            static let dashboard_home = "home"
            static let dashboard_bannerImage = "bannerImage"
            static let dashboard_posterImage = "posterImage"
            
            static let dashboard_name = "name"
            static let film_NameEng = "nameEng"
            static let film_year = "year"
            static let film_poster = "poster"
            static let film_uuid = "uuid"
            static let film_country = "country"
            static let film_description = "description"
            static let film_director = "director"
            static let film_imdb    = "imdb"
            static let film_name = "name"
            static let film_sugestions = "sugestions"
            static let film_time = "time"
            static let film_trailer = "trailer"
            static let film_type = "type"
            
        }
    }
}
