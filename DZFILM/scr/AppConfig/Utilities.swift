//
//  Utilities.swift
//  Enbac
//
//  Created by Hoang Duy Nam on 6/18/15.
//  Copyright (c) 2015 Hoang Duy Nam. All rights reserved.
//

import Foundation


class Utilities {
    
    
    class func numberFromJSONAnyObject(anyValue: AnyObject?) -> NSNumber?{
        if(anyValue == nil){
            return nil
        }
        
        if(anyValue is String){
            let intValue: NSNumber? = NSNumberFormatter().numberFromString((anyValue as! String))
            if(intValue != nil){
                return intValue!
            }
        }else if(anyValue is NSNumber){
            return anyValue as? NSNumber
        }
        
        return nil
    }
    
    class func getIntString(anyValue: AnyObject?) -> String {
        var intString = (anyValue as? String) ?? ""
        if(intString.isEmpty){
            intString = "\(Utilities.numberFromJSONAnyObject(anyValue)?.integerValue ?? 0)"
        }
        
        return intString
    }
    
    class func formatNumber(number: Int) -> String {
        let numberFormatter = NSNumberFormatter()
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.groupingSeparator = "."
        numberFormatter.groupingSize = 3
        return numberFormatter.stringFromNumber(number)!
    }
    
    class func formatDoubleNumber(number: Double) -> String {
        let numberFormatter = NSNumberFormatter()
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.groupingSeparator = "."
        numberFormatter.groupingSize = 3
        return numberFormatter.stringFromNumber(number)!
    }
    
    class func measureTextSize(text: String, font: UIFont) -> CGSize {
        let attributes = [NSFontAttributeName : font]
        
        return text.boundingRectWithSize(CGSizeMake(CGFloat.max, CGFloat.max), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attributes, context: nil).size
    }
    
    class func measureTextWidth(text: String, font: UIFont) -> CGFloat {
        let attributes = [NSFontAttributeName : font]
        
        return text.boundingRectWithSize(CGSizeMake(CGFloat.max, CGFloat.max), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attributes, context: nil).size.width
    }
    
    class func measureTextHeight(text: String, font: UIFont, maxWidth: CGFloat) -> CGFloat {
        let attributes = [NSFontAttributeName : font]
        
        return text.boundingRectWithSize(CGSizeMake(maxWidth, CGFloat.max), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attributes, context: nil).size.height
    }
    
    class func measureAttributedTextHeight(attrText: NSAttributedString, font: UIFont, maxWidth: CGFloat) -> CGFloat {
        let style = NSMutableParagraphStyle()
        style.lineBreakMode = NSLineBreakMode.ByWordWrapping
        
        let attrString = NSMutableAttributedString(attributedString: attrText)
        attrString.addAttribute(NSParagraphStyleAttributeName, value: style, range: NSMakeRange(0, attrText.length))
        
        let boundingRect = attrString.boundingRectWithSize(CGSizeMake(maxWidth, CGFloat.max), options: NSStringDrawingOptions.UsesLineFragmentOrigin, context: nil)
        return boundingRect.size.height
    }
    
    class func isEmail(email: String) -> Bool {
        
        let regex = try? NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@(?:[A-Z0-9-]+\\.)+[A-Z]{2,4}$", options: NSRegularExpressionOptions.CaseInsensitive)
        if let matches = regex?.matchesInString(email, options: NSMatchingOptions.ReportCompletion, range: NSMakeRange(0, email.characters.count)){
            return matches.count > 0
        }
        else{
            return false
        }
    }
    
    class func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        if(targetSize.width >= size.width && targetSize.height >= size.height){
            return image
        }
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}