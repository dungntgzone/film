//
//  BaseViewController.swift
//  DZFILM
//
//  Created by Nguyen Tien Dung on 9/21/15.
//  Copyright © 2015 Nguyen Tien Dung. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 21.0/255.0, green: 21.0/255.0, blue: 21.0/255.0, alpha: 1.0)
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationItem.hidesBackButton = true
        if let btnMenu = self.navigationController?.navigationBar.viewWithTag(1) as? JTHamburgerButton{
            btnMenu.hidden = self.navigationController?.viewControllers.count != 1
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
