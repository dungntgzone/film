//
//  LeftMenuViewController.swift
//  DZFILM
//
//  Created by Nguyen Tien Dung on 9/17/15.
//  Copyright © 2015 Nguyen Tien Dung. All rights reserved.
//

import UIKit

class LeftMenuViewController: UIViewController {

    //MARK: - 💤 LazyLoad Method
    private var mainNavigator : UINavigationController!
    private var btnMenu : JTHamburgerButton!
    //MARK: - ♻️ LifeCycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let barBackground = UINavigationBar(frame: self.view.bounds)
        barBackground.barStyle = UIBarStyle.BlackTranslucent
        self.view.addSubview(barBackground)
        self.view.sendSubviewToBack(barBackground)
        
        let dashboard = DashboardViewController(nibName: "DashboardViewController", bundle: nil)
        self.mainNavigator = UINavigationController(rootViewController: dashboard)
        self.mainNavigator.view.frame = self.view.bounds
        self.mainNavigator.navigationItem.hidesBackButton = true
        self.view.addSubview(self.mainNavigator.view)
        
        self.btnMenu = JTHamburgerButton(frame: CGRectMake(0, 0, 44, 44))
        self.btnMenu.addTarget(self, action: Selector("OnShowLeftMenu"), forControlEvents: UIControlEvents.TouchUpInside)
        self.btnMenu.setCurrentModeWithAnimation(JTHamburgerButtonMode.Hamburger)
        self.btnMenu.tag = 1
        self.mainNavigator.navigationBar.addSubview(self.btnMenu)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: - 🆑 BTN MENU Method
    
    func OnShowLeftMenu(){
        if(self.btnMenu.currentMode == .Hamburger){
        
            UIView.animateWithDuration(0.25, animations: { () -> Void in
                self.mainNavigator.view.frame = CGRectMake(self.view.bounds.size.width - 50, 0, self.view.bounds.size.width, self.view.bounds.size.height)
                
            }, completion: { (complete) -> Void in
               self.btnMenu.setCurrentModeWithAnimation(.Arrow, duration: 0.25)
            })
        }
        else{
            
            UIView.animateWithDuration(0.25, animations: { () -> Void in
                self.mainNavigator.view.frame = self.view.bounds
                }, completion: { (complete) -> Void in
                    self.btnMenu.setCurrentModeWithAnimation(.Hamburger, duration: 0.25)
            })
            
        }
        
    }
    
    //MARK: - 🆑 LOGIN FACEBOOK Method
    
    @IBAction func onFacebookLogin(sender: AnyObject) {
        PFFacebookUtils.logInInBackgroundWithReadPermissions(["public_profile","email"]) { (user, er) -> Void in
            print("Login");
        }
       
    }
}
