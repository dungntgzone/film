//
//  FilmViewController.swift
//  DZFILM
//
//  Created by Nguyen Tien Dung on 9/22/15.
//  Copyright © 2015 Nguyen Tien Dung. All rights reserved.
//

import UIKit

class FilmViewController: BaseViewController , UIScrollViewDelegate {

    /*
        67.5 * 102
    
    */
    
    //MARK: - 💤 LazyLoad Method
    var filmObj : PFObject!
    var sharebtn : UIBarButtonItem!
    var max_height_header : CGFloat!
    var url_trailers : String!
    
    @IBOutlet weak var mainSCR: UIScrollView!
    
    
    //MARK: - ♻️ LifeCycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        max_height_header = 0
        //self.navigationItem.title = filmObj[Constants.API.Codes.dashboard_name] as? String
        self.sharebtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Action, target: self, action: Selector("onShowShareAction"))
        self.navigationItem.rightBarButtonItem = self.sharebtn
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        MRProgressOverlayView.showOverlayAddedTo(self.view, title: "Đang tải", mode: MRProgressOverlayViewMode.IndeterminateSmallDefault, animated: true)
        APIFilmData.GetFillDetail(filmObj[Constants.API.Codes.film_uuid] as! String, success: { (data) -> Void in
            self.filmObj = data
            self.updateDisplay()
            MRProgressOverlayView.dismissAllOverlaysForView(self.view, animated: true)
            }) { (erMessage) -> Void in
                MRProgressOverlayView.dismissAllOverlaysForView(self.view, animated: true)
                self.navigationController?.view.makeToast(erMessage, duration: 2, position: CSToastPositionCenter)
                self.navigationController?.popViewControllerAnimated(true)
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK: - 🎬 Actions onShowShareAction
    func onShowShareAction(){
        
    }
    
    //MARK: - 🆑 Custom Number To String
    func convertNumberToString(number : Int) -> String{
        if(number < 10){
            return "0\(number)"
        }
        else{
            return "\(number)"
        }
    }
    
    //MARK: - 🎬 Actions ArrayString To String
    func convertArrayStringToString(arr : Array<String>) -> String{
        var str = ""
        for i in 0..<arr.count{
            if(i == arr.count - 1 ){
                str = str + "\(arr[i])"
            }
            else{
                str = str + "\(arr[i]),"
            }
        }
        return str
    }
    
    //MARK: - 🎬 Actions on Display
    
    func updateDisplay(){
        //imgPoster
        if let url_imgPoster = filmObj[Constants.API.Codes.film_poster] as? String{
            let imgPoster = UIImageView(frame: CGRectMake(10, 10, 71 * Constants.System.scare, 102 * Constants.System.scare))
            self.mainSCR.addSubview(imgPoster)
            imgPoster.sd_setImageWithURL(NSURL(string: url_imgPoster))
            max_height_header = 102 * Constants.System.scare + 20
        }
        // film Vie Name // Eng Name // time / imdb
        var vieName = ""
        var EngName = ""
        var imdbString = ""
        var timeString = ""
        
        
        if let filmNameString = filmObj[Constants.API.Codes.film_name] as? String{
            vieName = filmNameString
        }
        if let filmEngString = filmObj[Constants.API.Codes.film_NameEng] as? String{
            EngName = filmEngString
        }
        if let imdb_film = filmObj[Constants.API.Codes.film_imdb] as? String{
            imdbString = imdb_film
        }
        if let time_film = filmObj[Constants.API.Codes.film_time] as? String{
            timeString = time_film
        }
        
        
        
        let attFilmName = NSMutableAttributedString(string: vieName + "\n" + EngName, attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(16),NSForegroundColorAttributeName:UIColor.whiteColor()])
        if(!imdbString.isEmpty){
            
            attFilmName.appendAttributedString(NSMutableAttributedString(string: "\nIMDB : " + imdbString, attributes: [NSFontAttributeName:UIFont.systemFontOfSize(14),NSForegroundColorAttributeName:UIColor(red: 143.0/255.0, green: 143.0/255.0, blue: 143.0/255.0, alpha: 1.0)]))
        }
        if(!timeString.isEmpty){
            attFilmName.appendAttributedString(NSMutableAttributedString(string: "\nThời lượng : " + timeString, attributes: [NSFontAttributeName:UIFont.systemFontOfSize(14),NSForegroundColorAttributeName:UIColor(red: 143.0/255.0, green: 143.0/255.0, blue: 143.0/255.0, alpha: 1.0)]))
        }
        let sizeFilmName = attFilmName.boundingRectWithSize(CGSizeMake(Constants.System.screenSize.width - 30 - 71 * Constants.System.scare, CGFloat.max), options:.UsesLineFragmentOrigin, context: nil)
        if(sizeFilmName.height > (102 * Constants.System.scare)){
            self.max_height_header = sizeFilmName.height + 20
        }
        
        let lblFilmName = UILabel(frame: CGRectMake(20 + 71 * Constants.System.scare, 10, Constants.System.screenSize.width - 30 - 71 * Constants.System.scare, sizeFilmName.height))
        lblFilmName.lineBreakMode = NSLineBreakMode.ByWordWrapping
        lblFilmName.numberOfLines = 0
        lblFilmName.attributedText = attFilmName
        self.mainSCR.addSubview(lblFilmName)
        let bar = UINavigationBar(frame: CGRectMake(0, self.max_height_header, Constants.System.screenSize.width, 44))
        bar.tag = 1
        bar.translucent = true
        
        let barMenu = UISegmentedControl(items: ["Chi tiết","Tập","Gợi ý"])
        barMenu.frame = CGRectMake(10, 7, Constants.System.screenSize.width - 20, 30)
        barMenu.selectedSegmentIndex = 0
        barMenu.tintColor = UIColor(red: 91.0/255.0, green: 91.0/255.0, blue: 91.0/255.0, alpha: 1.0)
        barMenu.addTarget(self, action: Selector("barMenuSelected:"), forControlEvents: UIControlEvents.ValueChanged)
        bar.addSubview(barMenu)
    
        self.mainSCR.addSubview(bar)
        
        
        // button play now
        
        let barAction = UISegmentedControl(items: ["Xem Phim"])
        barAction.frame = CGRectMake(Constants.System.screenSize.width - 110, bar.frame.origin.y - 35, 100, 30)
        barAction.tintColor = UIColor.redColor()
        self.mainSCR.addSubview(barAction)
        
        // description
        var currentH = self.max_height_header + 54
        
        if let film_description =  self.filmObj.objectForKey(Constants.API.Codes.film_description) as? String{
            if(!film_description.isEmpty){
                let desctiption_title = UILabel(frame:CGRectMake(10,currentH,Constants.System.screenSize.width - 20,20))
                desctiption_title.font = UIFont.boldSystemFontOfSize(16)
                desctiption_title.textColor = UIColor.whiteColor()
                desctiption_title.text = "Nội dung phim"
                mainSCR.addSubview(desctiption_title)
                
                currentH += 30
                
                let desctiption_value_att = NSAttributedString(string: film_description, attributes: [NSFontAttributeName:UIFont.systemFontOfSize(14),NSForegroundColorAttributeName:UIColor(red: 143.0/255.0, green: 143.0/255.0, blue: 143.0/255.0, alpha: 1.0)])
                let desctiption_value_size = desctiption_value_att.boundingRectWithSize(CGSizeMake(Constants.System.screenSize.width - 20,CGFloat.max), options: .UsesLineFragmentOrigin, context: nil)
                
                let desctiption_value = UILabel(frame:CGRectMake(10,currentH,Constants.System.screenSize.width - 20,desctiption_value_size.height))
                desctiption_value.lineBreakMode = NSLineBreakMode.ByWordWrapping
                desctiption_value.numberOfLines = 0
                desctiption_value.attributedText = desctiption_value_att
                self.mainSCR.addSubview(desctiption_value)
                
                currentH += desctiption_value_size.height + 10
                
                
                
                // add line
                let img_for_descriotion = UIImageView(frame: CGRectMake(10, currentH-1, Constants.System.screenSize.width-10, 0.5))
                img_for_descriotion.backgroundColor = UIColor(red: 47.0/255.0, green: 47.0/255.0, blue: 47.0/255.0, alpha: 1.0)
                self.mainSCR.addSubview(img_for_descriotion)
                
                currentH += 10
            }
            
            
        }
        
        // trailer
        if let film_trailer =  self.filmObj.objectForKey(Constants.API.Codes.film_trailer) as? String{
            if(!film_trailer.isEmpty){
                let trailers_title = UILabel(frame:CGRectMake(10,currentH,Constants.System.screenSize.width - 20,20))
                trailers_title.font = UIFont.boldSystemFontOfSize(16)
                trailers_title.textColor = UIColor.whiteColor()
                trailers_title.text = "Trailers"
                mainSCR.addSubview(trailers_title)
                
                currentH += 30
                
                let btn_trailers = UIButton()
                
                
                
                self.mainSCR.addSubview(btn_trailers)
                self.mainSCR.sendSubviewToBack(btn_trailers)
                HCYoutubeParser.thumbnailForYoutubeID(film_trailer, thumbnailSize: YouTubeThumbnailDefaultMedium, completeBlock: { (img : UIImage!, er : NSError!) -> Void in
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        if(img != nil){
                            // Tính toán w / h theo h = 65
                            let detal = 65 / img.size.height
                            btn_trailers.frame = CGRectMake(10, trailers_title.frame.origin.y + 30, img.size.width * detal, 65)
                            btn_trailers.setBackgroundImage(img, forState: .Normal)
                        }
                    })
                })
                
                currentH += 75
                
                
                
                let trailers_value = UILabel(frame:CGRectMake(10,currentH,Constants.System.screenSize.width - 20,40))
                trailers_value.font = UIFont.boldSystemFontOfSize(16)
                trailers_value.textColor = UIColor.whiteColor()
                trailers_value.numberOfLines = 0
                trailers_value.text = "Trailers"
                trailers_value.lineBreakMode = NSLineBreakMode.ByWordWrapping
                mainSCR.addSubview(trailers_value)
                
                
                currentH += 50
                
                
                // add line
                let img_for_descriotion = UIImageView(frame: CGRectMake(10, currentH-1, Constants.System.screenSize.width-10, 0.5))
                img_for_descriotion.backgroundColor = UIColor(red: 47.0/255.0, green: 47.0/255.0, blue: 47.0/255.0, alpha: 1.0)
                self.mainSCR.addSubview(img_for_descriotion)
                
                currentH += 10
                
                let youtubeURLString = "https://www.youtube.com/watch?v=\(film_trailer)"
                let youtubeURL = NSURL(string: youtubeURLString)
                HCYoutubeParser.h264videosWithYoutubeURL(youtubeURL, completeBlock: { (data, er) -> Void in
                    
                    if let metaData = data as? Dictionary<String,AnyObject>{
                        if let moreInfo = metaData["moreInfo"] as? Dictionary<String,AnyObject>{
                            if let time = Utilities.numberFromJSONAnyObject(moreInfo["length_seconds"]){
                                let hour = time.integerValue / 3600
                                let minute = (time.integerValue / 60) % 60
                                let second = time.integerValue % 60
                                
                                var timeString = "\(self.convertNumberToString(second))"
                                if(minute == 0){
                                    timeString = "00:\(timeString)"
                                }
                                else{
                                    timeString = "\(self.convertNumberToString(minute)):\(timeString)"
                                    if(hour != 0){
                                        timeString = "\(self.convertNumberToString(hour)):\(timeString)"
                                    }
                                    
                                }
                                let att_time = NSAttributedString(string: "Trailers\n\(timeString)", attributes: [NSFontAttributeName:UIFont.systemFontOfSize(14),NSForegroundColorAttributeName:UIColor(red: 143.0/255.0, green: 143.0/255.0, blue: 143.0/255.0, alpha: 1.0)])
                                trailers_value.attributedText = att_time
                                
                            }
                        }
                        
                        btn_trailers.setImage(UIImage(named: "ic_play")?.tintWithColor(UIColor.whiteColor()), forState: .Normal)
                        btn_trailers.addTarget(self, action: Selector("onPlayTrailers"), forControlEvents: UIControlEvents.TouchUpInside)
                        
                        if let hd720 = metaData["hd720"] as? String{
                            self.url_trailers = hd720
                        }
                        else if let medium = metaData["medium"] as? String{
                            self.url_trailers = medium
                        }
                        else if let small = metaData["small"] as? String{
                            self.url_trailers = small
                        }
                        
                    }
                    else{
                        trailers_value.text = "Lỗi không tải được"
                        btn_trailers.setBackgroundImage(UIImage(named: "meh7-vflGevej7"), forState: .Normal)
                    }
                    
                    
                    
                })
            }
            
            
        }
        // Thông tin
        
        let desctiption_title = UILabel(frame:CGRectMake(10,currentH,Constants.System.screenSize.width - 20,20))
        desctiption_title.font = UIFont.boldSystemFontOfSize(16)
        desctiption_title.textColor = UIColor.whiteColor()
        desctiption_title.text = "Thông tin"
        mainSCR.addSubview(desctiption_title)
        
        currentH += 30
        
        var metadataString = ""
        if let year = self.filmObj[Constants.API.Codes.film_year] as? String{
            metadataString =  "\(metadataString)Năm sản xuất : \(year)\n"
        }
        if let directors = self.filmObj[Constants.API.Codes.film_director] as? Array<String>{
            if(directors.count != 0){
                metadataString = "\(metadataString)Đạo diễn : \(convertArrayStringToString(directors))\n"
            }
        }
        if let country = self.filmObj[Constants.API.Codes.film_country] as? Array<String>{
            if(country.count != 0){
                metadataString = "\(metadataString)Quốc gia : \(convertArrayStringToString(country))\n"
            }
        }
        if let types = self.filmObj[Constants.API.Codes.film_type] as? Array<String>{
            if(types.count != 0){
                metadataString = "\(metadataString)Thể loại : \(convertArrayStringToString(types))\n"
            }
        }
        if let timess = self.filmObj[Constants.API.Codes.film_time] as? String{
            metadataString = "\(metadataString)Thời lượng : \(timess)\n"
        }
        let metadata_style = NSMutableParagraphStyle()
        metadata_style.lineSpacing = 3.0
        
        let metadata_value_att = NSAttributedString(string: metadataString, attributes: [NSFontAttributeName:UIFont.systemFontOfSize(14),NSForegroundColorAttributeName:UIColor(red: 143.0/255.0, green: 143.0/255.0, blue: 143.0/255.0, alpha: 1.0),NSParagraphStyleAttributeName:metadata_style])
        let metadata_value_size = metadata_value_att.boundingRectWithSize(CGSizeMake(Constants.System.screenSize.width - 20,CGFloat.max), options: .UsesLineFragmentOrigin, context: nil)
        
        let metadata_value = UILabel(frame:CGRectMake(10,currentH,Constants.System.screenSize.width - 20,metadata_value_size.height))
        metadata_value.lineBreakMode = NSLineBreakMode.ByWordWrapping
        metadata_value.numberOfLines = 0
        metadata_value.attributedText = metadata_value_att
        self.mainSCR.addSubview(metadata_value)
        
        currentH += metadata_value_size.height + 10
        
        // add line
        let img_for_Metadata = UIImageView(frame: CGRectMake(10, currentH-1, Constants.System.screenSize.width-10, 0.5))
        img_for_Metadata.backgroundColor = UIColor(red: 47.0/255.0, green: 47.0/255.0, blue: 47.0/255.0, alpha: 1.0)
        self.mainSCR.addSubview(img_for_Metadata)
        
        currentH += 10
        
        // Bản Quyền 
        
        let Licence_title = UILabel(frame:CGRectMake(10,currentH,Constants.System.screenSize.width - 20,20))
        Licence_title.font = UIFont.boldSystemFontOfSize(16)
        Licence_title.textColor = UIColor.whiteColor()
        Licence_title.text = "Tác giả"
        mainSCR.addSubview(Licence_title)
        
        currentH += 30
        
        let Licence_value_att = NSAttributedString(string: "Mọi thông tin trên đều được chúng tôi tự động thu thập trên internet và chúng tôi không có bất cứ liên hệ với tác giả của bộ phim này!", attributes: [NSFontAttributeName:UIFont.systemFontOfSize(11),NSForegroundColorAttributeName:UIColor(red: 143.0/255.0, green: 143.0/255.0, blue: 143.0/255.0, alpha: 1.0),NSParagraphStyleAttributeName:metadata_style])
        let Licence_value_size = Licence_value_att.boundingRectWithSize(CGSizeMake(Constants.System.screenSize.width - 20,CGFloat.max), options: .UsesLineFragmentOrigin, context: nil)
        
        let Licence_value = UILabel(frame:CGRectMake(10,currentH,Constants.System.screenSize.width - 20,Licence_value_size.height))
        Licence_value.lineBreakMode = NSLineBreakMode.ByWordWrapping
        Licence_value.numberOfLines = 0
        Licence_value.attributedText = Licence_value_att
        self.mainSCR.addSubview(Licence_value)
        
        currentH += Licence_value_size.height + 10
        
        
        self.mainSCR.bringSubviewToFront(bar)
        
        
        self.mainSCR.contentSize = CGSizeMake(Constants.System.screenSize.width, currentH)
    }
    
    
    //MARK: - 🔌 UIScrollViewDelegate Method
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if((scrollView.contentOffset.y + 64) >= self.max_height_header){
            let deltaY = (scrollView.contentOffset.y + 64) - self.max_height_header
            if let viewBar = self.mainSCR.viewWithTag(1) as? UINavigationBar{
                viewBar.frame = CGRectMake(0,self.max_height_header +  deltaY, Constants.System.screenSize.width, 44)
            }
        }
        else{
            if let viewBar = self.mainSCR.viewWithTag(1) as? UINavigationBar{
                viewBar.frame = CGRectMake(0,self.max_height_header, Constants.System.screenSize.width, 44)
            }
        }
    }
    
    //MARK: - 🎬 Actions Bar Change
    func barMenuSelected(sender : UISegmentedControl){
        if(sender.selectedSegmentIndex == 1 || sender.selectedSegmentIndex == 2){
            self.mainSCR.setContentOffset(CGPointMake(0, -64), animated: true)
            self.mainSCR.scrollEnabled = false
        }
        else{
            self.mainSCR.scrollEnabled = true
        }
        
    }
    
    //MARK: - 🎬 Actions Play Trailers
    func onPlayTrailers(){
        if(self.url_trailers != nil){
            
        }
    }
}
