//
//  DashboadBannerCell.swift
//  DZFILM
//
//  Created by Nguyen Tien Dung on 9/18/15.
//  Copyright © 2015 Nguyen Tien Dung. All rights reserved.
//

import UIKit

class DashboadBannerCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource {

    //MARK: - 💤 LazyLoad Method
    @IBOutlet weak var tblCollectionBanner: UICollectionView!
    
    var onFilmSelected : ((obj : PFObject) -> Void)!
    
    private var tblBanner: UITableView!
    //MARK: - ♻️ LifeCycle Method
    override func awakeFromNib() {
        super.awakeFromNib()
        if(tblBanner == nil){
            self.tblBanner = UITableView()
            self.addSubview(self.tblBanner)
            self.tblBanner.translatesAutoresizingMaskIntoConstraints = false
            self.tblBanner.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI_2))
            self.tblBanner.frame = CGRectMake(0, 0, Constants.System.screenSize.width, 120 * Constants.System.scare)
            self.tblBanner.dataSource = self
            self.tblBanner.scrollEnabled = true
            self.tblBanner.backgroundColor = UIColor.clearColor()
            self.tblBanner.pagingEnabled = true
            self.tblBanner.showsVerticalScrollIndicator = false
            self.tblBanner.delegate = self
            self.tblBanner.separatorColor = UIColor.clearColor()
        }
        
        
        
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - 🔌 UITableViewDelegate,UITableViewDataSource Method
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (AppShareModel.sharedInstance().listPanner == nil) ? 0 : AppShareModel.sharedInstance().listPanner.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return Constants.System.screenSize.width
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell:banner")
        if(cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell:banner")
            let imgBanner = UIImageView()
            imgBanner.frame = CGRectMake(0, 0, Constants.System.screenSize.width, 120 * Constants.System.scare)
            imgBanner.tag = 1
            cell?.contentView.addSubview(imgBanner)
            
            let viewBar  = UIView(frame: CGRectMake(0, 120 * Constants.System.scare - 30, Constants.System.screenSize.width, 30))
            viewBar.backgroundColor = UIColor(white: 0, alpha: 0.65)
            cell?.contentView.addSubview(viewBar)
            
            let lblFilmName = UILabel(frame: CGRectMake(15, 120 * Constants.System.scare - 30, Constants.System.screenSize.width - 30, 30))
            lblFilmName.tag = 2
            lblFilmName.font = UIFont.systemFontOfSize(14)
            lblFilmName.textColor = UIColor.whiteColor()
            cell?.contentView.addSubview(lblFilmName)
            
        }
        cell?.contentView.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_2))
        cell?.contentView.backgroundColor = UIColor.clearColor()
        cell?.selectionStyle = .None
        if let imgBanner = cell?.contentView.viewWithTag(1) as? UIImageView{
            if let bannerURL = AppShareModel.sharedInstance().listPanner[indexPath.row][Constants.API.Codes.dashboard_bannerImage] as? String{
                imgBanner.sd_setImageWithURL(NSURL(string: bannerURL)!)
            }
        }
        if let lblFilmName = cell?.contentView.viewWithTag(2) as? UILabel{
            if let filmName = AppShareModel.sharedInstance().listPanner[indexPath.row][Constants.API.Codes.dashboard_name] as? String{
                lblFilmName.text = filmName
            }
        }
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let obj = AppShareModel.sharedInstance().listPanner[indexPath.row]
        if(self.onFilmSelected != nil){
            self.onFilmSelected!(obj: obj)
        }

    }
    //MARK: - 🆑 RELOAD Method
    func reloadData(){
        self.tblBanner.reloadData()
    }
    //MARK: - 🆑 Next Banner Method
    func nextBanner(){
        if let rows = self.tblBanner.indexPathsForVisibleRows{
            if(rows.count != 0){
                if let index = rows.first{
                    if(index.row == AppShareModel.sharedInstance().listPanner.count - 1){
                        tblBanner.scrollToRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), atScrollPosition: UITableViewScrollPosition.Top, animated: true)
                    }
                    else{
                        tblBanner.scrollToRowAtIndexPath(NSIndexPath(forRow: index.row + 1, inSection: index.section), atScrollPosition: UITableViewScrollPosition.None, animated: true)
                    }
                }
            }
        }
    }
}
