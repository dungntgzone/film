//
//  DashboardFilmCell.swift
//  DZFILM
//
//  Created by Nguyen Tien Dung on 9/21/15.
//  Copyright © 2015 Nguyen Tien Dung. All rights reserved.
//

import UIKit

class DashboardFilmCell: UITableViewCell,UITableViewDataSource,UITableViewDelegate {

    //MARK: - 💤 LazyLoad Method
    private var tblCellFilm : UITableView!
    var cellIndex : Int = -1
    @IBOutlet weak var lblFilmGroupName: UILabel!
    var onFilmSelected : ((obj : PFObject) -> Void)!
    var onGroupSelected : ((grname : String,groupList : Array<PFObject>) -> Void)!
    //MARK: - ♻️ LifeCycle Method
    override func awakeFromNib() {
        super.awakeFromNib()
         // 80 + 15 PADING
        // 115 + 45 = 160
        // Initialization code
        
        self.tblCellFilm = UITableView()
        self.addSubview(self.tblCellFilm)
        self.tblCellFilm.translatesAutoresizingMaskIntoConstraints = false
        self.tblCellFilm.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI_2))
        self.tblCellFilm.frame = CGRectMake(0, 45, Constants.System.screenSize.width, 159.5)
        self.tblCellFilm.dataSource = self
        self.tblCellFilm.delegate = self
        self.tblCellFilm.backgroundColor = UIColor.clearColor()
        self.tblCellFilm.showsVerticalScrollIndicator = false
        self.tblCellFilm.separatorColor = UIColor.clearColor()
        self.tblCellFilm.contentInset = UIEdgeInsetsMake(15, 0, 15, 0)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    //MARK: - 🔌 UITableViewDataSource,UITableViewDelegate Method
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (cellIndex == -1) ? 0 : AppShareModel.sharedInstance().listHome[cellIndex][AppShareModel.sharedInstance().listHome[cellIndex].keys.first!]!.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 95
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell:film")
        if(cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell:film")
            let imgPoster = UIImageView()
            imgPoster.translatesAutoresizingMaskIntoConstraints = false
            imgPoster.frame = CGRectMake(0, 0, 80, 115)
            imgPoster.layer.borderWidth = 1
            imgPoster.layer.borderColor = UIColor.lightGrayColor().CGColor
            imgPoster.tag = 1
            cell!.contentView.addSubview(imgPoster)
            
            let lblFilmName = UILabel(frame: CGRectMake(0, 115, 80, 45))
            lblFilmName.lineBreakMode = NSLineBreakMode.ByWordWrapping
            lblFilmName.numberOfLines = 2
            lblFilmName.textColor = UIColor.whiteColor()
            lblFilmName.font = UIFont.systemFontOfSize(14)
            lblFilmName.textAlignment = .Left
            lblFilmName.tag = 2
            cell?.contentView.addSubview(lblFilmName)
        }
        cell!.contentView.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_2))
        cell!.backgroundColor = UIColor.clearColor()
        cell!.selectionStyle = .None
        if let filmObj = AppShareModel.sharedInstance().listHome[cellIndex][AppShareModel.sharedInstance().listHome[cellIndex].keys.first!]![indexPath.row]{
            if let imgPoster = cell?.contentView.viewWithTag(1) as? UIImageView{
                if let bannerURL = filmObj[Constants.API.Codes.dashboard_posterImage] as? String{
                    imgPoster.sd_setImageWithURL(NSURL(string: bannerURL)!)
                }
            }
            if let lblFilmName = cell?.contentView.viewWithTag(2) as? UILabel{
                if let textFilmName = filmObj[Constants.API.Codes.dashboard_name] as? String{
                    lblFilmName.text = textFilmName
                }
            }
        }
        
        return cell!
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let filmObj = AppShareModel.sharedInstance().listHome[cellIndex][AppShareModel.sharedInstance().listHome[cellIndex].keys.first!]![indexPath.row] as? PFObject{
            if(self.onFilmSelected != nil){
                self.onFilmSelected!(obj: filmObj)
            }
        }
    }
    //MARK: - 🆑 RELOAD Method
    func reloadData(){
        self.tblCellFilm.reloadData()
        self.tblCellFilm.scrollsToTop = true
        if(cellIndex != -1){
            self.lblFilmGroupName.text = AppShareModel.sharedInstance().listHome[cellIndex].keys.first
        }
    }

    
    @IBAction func onViewAll(sender: AnyObject) {
        if let filmsObj = AppShareModel.sharedInstance().listHome[cellIndex][AppShareModel.sharedInstance().listHome[cellIndex].keys.first!] as? [PFObject]{
            if(self.onFilmSelected != nil){
                self.onGroupSelected!(grname: AppShareModel.sharedInstance().listHome[cellIndex].keys.first!,groupList: filmsObj)
            }
        }
    }
    
}
