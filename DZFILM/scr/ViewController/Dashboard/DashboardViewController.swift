//
//  DashboardViewController.swift
//  DZFILM
//
//  Created by Nguyen Tien Dung on 9/17/15.
//  Copyright © 2015 Nguyen Tien Dung. All rights reserved.
//

import UIKit

class DashboardViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {
    //MARK: - 💤 LazyLoad Method
    @IBOutlet weak var tblDashboard: UITableView!
    private var timmerBanner : NSTimer!
    private var searchBar : UISearchBar!
    private var searchBarButton : UIBarButtonItem!
    //MARK: - ♻️ LifeCycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblDashboard.registerNib(UINib(nibName: "DashboadBannerCell", bundle: nil), forCellReuseIdentifier: "DashboadBannerCell")
        self.tblDashboard.registerNib(UINib(nibName: "DashboardFilmCell", bundle: nil), forCellReuseIdentifier: "DashboardFilmCell")
        self.tblDashboard.showsVerticalScrollIndicator = false
        self.navigationItem.title = "Home"
        
        self.searchBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Search, target: self, action: Selector("onSearchFilm"))
        self.navigationItem.rightBarButtonItem = self.searchBarButton
        
        self.searchBar = UISearchBar()
        self.searchBar.showsCancelButton = true
        self.searchBar.placeholder = "Tìm kiếm phim"
        self.searchBar.keyboardAppearance = UIKeyboardAppearance.Dark
        self.searchBar.delegate = self
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if(AppShareModel.sharedInstance().listHome == nil){
            MRProgressOverlayView.showOverlayAddedTo(self.view, title: "Đang tải...", mode: MRProgressOverlayViewMode.IndeterminateSmallDefault, animated: true)
            APIFilmData.HomePageData({ (data) -> Void in
                MRProgressOverlayView.dismissAllOverlaysForView(self.view, animated: true)
                self.tblDashboard.reloadData()
                if(self.timmerBanner == nil){
                    // start timmer
                    self.timmerBanner = NSTimer.scheduledTimerWithTimeInterval(6, target: self, selector: Selector("OnNextBanner"), userInfo: nil, repeats: true)
                }
                }) { (erMessage) -> Void in
                    MRProgressOverlayView.dismissAllOverlaysForView(self.view, animated: true)
                    self.view.makeToast(erMessage, duration: 2, position: CSToastPositionCenter)
            }
        }
        
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeSearchBar()
    }
    //MARK: - 🔌 UITableViewDataSource,UITableViewDelegate  Method
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return (AppShareModel.sharedInstance().listHome != nil) ? AppShareModel.sharedInstance().listHome.count + 1 : 0
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section == 0) ? 1 : 1
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return (indexPath.section == 0) ? 120 * Constants.System.scare : 205
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCellWithIdentifier("DashboadBannerCell") as! DashboadBannerCell
            cell.backgroundColor = UIColor.clearColor()
            cell.reloadData()
            weak var _self = self
            cell.onFilmSelected = {(objFilm : PFObject) -> Void in
                let film = FilmViewController(nibName: "FilmViewController", bundle: nil)
                film.filmObj = objFilm
                _self?.navigationController?.pushViewController(film, animated: true)
            }
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCellWithIdentifier("DashboardFilmCell") as! DashboardFilmCell
            cell.backgroundColor = UIColor.clearColor()
            cell.selectionStyle = .None
            cell.cellIndex = indexPath.section - 1
            cell.reloadData()
            weak var _self = self
            cell.onGroupSelected = {(grname : String,groupList : Array<PFObject>) -> Void in
                let gr = GroupViewController(nibName: "GroupViewController", bundle: nil)
                gr.groupName = grname
                gr.listGroup = groupList
                _self?.navigationController?.pushViewController(gr, animated: true)
            }
            cell.onFilmSelected = {(objFilm : PFObject) -> Void in
                let film = FilmViewController(nibName: "FilmViewController", bundle: nil)
                film.filmObj = objFilm
                _self?.navigationController?.pushViewController(film, animated: true)
            }
            return cell
        }
    }
    //MARK: - 🆑 OnNextBanner Method
    func OnNextBanner(){
        
        if let cell = self.tblDashboard.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as? DashboadBannerCell{
            cell.nextBanner()
        }
    }
    
    //MARK: - 🆑 Search  Method
    func onSearchFilm(){
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.titleView = self.searchBar;
        self.searchBar.alpha = 0
        UIView.animateWithDuration(0.25, animations: { () -> Void in
            self.searchBar.alpha = 1
            }, completion: { (complete) -> Void in
                self.searchBar.becomeFirstResponder()
        })
    }
    
    
    //MARK: - 🔌 UISearchBarDelegate Method
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.removeSearchBar()
    }
    func removeSearchBar(){
        self.navigationItem.rightBarButtonItem = self.searchBarButton
        UIView.animateWithDuration(0.25, animations: { () -> Void in
            self.navigationItem.titleView?.alpha = 0
            self.searchBar.text = ""
            }) { (complete) -> Void in
                self.navigationItem.titleView = nil
                self.navigationItem.rightBarButtonItem = self.searchBarButton
        }
    }
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        if(searchBar.text != nil && !searchBar.text!.isEmpty){
            MRProgressOverlayView.showOverlayAddedTo(self.view, title: "Đang tìm kiếm...", mode: MRProgressOverlayViewMode.IndeterminateSmallDefault, animated: true)

            APIFilmData.SearchFilm(20, skip: 0, searchtext: searchBar.text!.lowercaseString, success: { (data : Array<PFObject>) -> Void in
                MRProgressOverlayView.dismissAllOverlaysForView(self.view, animated: true)
                if(data.count == 0){
                    self.view.makeToast("Không tìm thấy phim yêu cầu", duration: 2, position: CSToastPositionCenter)
                }
                else{
                    self.removeSearchBar()
                    let gr = GroupViewController(nibName: "GroupViewController", bundle: nil)
                    gr.groupName = "Tìm kiếm"
                    gr.listGroup = data
                    gr.isShowSearch = true
                    gr.searchText = searchBar.text!.lowercaseString
                    self.navigationController?.pushViewController(gr, animated: true)
                }
                
            }) { (erMessage) -> Void in
                MRProgressOverlayView.dismissAllOverlaysForView(self.view, animated: true)
                self.view.makeToast(erMessage, duration: 2, position: CSToastPositionCenter)
            }
        }
        
    }
    
}
