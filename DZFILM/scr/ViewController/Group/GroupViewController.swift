//
//  GroupViewController.swift
//  DZFILM
//
//  Created by Nguyen Tien Dung on 9/22/15.
//  Copyright © 2015 Nguyen Tien Dung. All rights reserved.
//

import UIKit

class GroupViewController: BaseViewController , UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate {
    //MARK: - 💤 LazyLoad Method
    
    @IBOutlet weak var tblGroupFilm: UITableView!
    
    var groupName : String = ""
    var listGroup : Array<PFObject>!
    var isShowSearch : Bool = false
    var searchText : String = ""
    
    private var className : String = ""
    private var loadDone : Bool = false,isLoading = false
    private var pageCount : Int = 20,skip = 0
    private var loadMoreControl : UIActivityIndicatorView!
    
    //MARK: - ♻️ LifeCycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = groupName
        self.tblGroupFilm.registerNib(UINib(nibName: "FilmInGroupTableViewCell", bundle: nil), forCellReuseIdentifier: "FilmInGroupTableViewCell")
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if(self.loadMoreControl == nil){
            self.skip = self.listGroup.count
            if(self.listGroup.count >= pageCount){
                self.createLoadMore()
            }
        }
        self.tblGroupFilm.reloadData()
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: - 🆑 Custom Method
    
    //MARK: - 🔌 UITableViewDataSource,UITableViewDelegate Method
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.listGroup == nil) ? 0 : self.listGroup.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : FilmInGroupTableViewCell = tableView.dequeueReusableCellWithIdentifier("FilmInGroupTableViewCell") as! FilmInGroupTableViewCell
        let film = self.listGroup[indexPath.row]
        self.className = film.parseClassName
        if let film_posterURL = film[Constants.API.Codes.dashboard_posterImage] as? String{
            if let urlEncode = film_posterURL.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet()){
                cell.imgPoster.sd_setImageWithURL(NSURL(string: urlEncode))
            }
        }
        else{
            if let film_posterURL = film[Constants.API.Codes.film_poster] as? String{
                if let urlEncode = film_posterURL.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet()){
                    cell.imgPoster.sd_setImageWithURL(NSURL(string: urlEncode))
                }
            }
        }
        cell.backgroundColor = UIColor.clearColor()
        cell.accessoryType = .DisclosureIndicator
        let attInfo = NSMutableAttributedString(string: "")
        var filmName_temp = ""
        if let filmName = film[Constants.API.Codes.dashboard_name] as? String{
            filmName_temp = filmName
            attInfo.appendAttributedString(NSAttributedString(string: filmName, attributes: [NSFontAttributeName:UIFont.systemFontOfSize(15),NSForegroundColorAttributeName:UIColor.whiteColor()]))
        }
        if let filmNameEng = film[Constants.API.Codes.film_NameEng] as? String{
            if(filmName_temp != filmNameEng){
                attInfo.appendAttributedString(NSAttributedString(string: "\n" + filmNameEng, attributes: [NSFontAttributeName:UIFont.systemFontOfSize(13),NSForegroundColorAttributeName:UIColor(red: 113.0/255.0, green: 113.0/255.0, blue: 113.0/255.0, alpha: 1)]))
                
            }
        }
        if let filmNameIMDB = film[Constants.API.Codes.film_imdb] as? String{
            if(filmNameIMDB != "0"){
                attInfo.appendAttributedString(NSAttributedString(string: "\nĐánh giá : \(filmNameIMDB)/10", attributes: [NSFontAttributeName:UIFont.systemFontOfSize(13),NSForegroundColorAttributeName:UIColor.whiteColor()]))
            }
        }
        if let filmNameYear = film[Constants.API.Codes.film_year] as? String{
            attInfo.appendAttributedString(NSAttributedString(string: "\nNăm sản xuất : " + filmNameYear, attributes: [NSFontAttributeName:UIFont.systemFontOfSize(13),NSForegroundColorAttributeName:UIColor.whiteColor()]))
        }
        cell.lblInfo.attributedText = attInfo
        
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let film = self.listGroup[indexPath.row]
        let filmDetail = FilmViewController(nibName: "FilmViewController", bundle: nil)
        filmDetail.filmObj = film
        self.navigationController?.pushViewController(filmDetail, animated: true)
    }
    
    //MARK: - 🆑 LOAD MORE Method
    
    func createLoadMore(){
        if(self.loadMoreControl == nil){
            let loadMoreFrameHeight: CGFloat = 44
            let footerView = UIView(frame: CGRectMake(0, 0, self.tblGroupFilm.frame.size.width, loadMoreFrameHeight))
            footerView.backgroundColor = UIColor.clearColor()
            footerView.autoresizingMask = UIViewAutoresizing.FlexibleWidth
            
            self.loadMoreControl = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
            self.loadMoreControl!.center = CGPointMake(footerView.frame.size.width/2.0, loadMoreFrameHeight/2.0)
            self.loadMoreControl!.autoresizingMask = [.FlexibleLeftMargin,.FlexibleRightMargin]
            self.loadMoreControl!.hidesWhenStopped = true
            self.loadMoreControl!.tintColor = UIColor.whiteColor()
            footerView.addSubview(self.loadMoreControl!)
            self.loadMoreControl!.stopAnimating()
            self.tblGroupFilm.tableFooterView = footerView;
        }
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if(!self.isLoading && !self.loadDone && self.loadMoreControl != nil) {
            self.loadMoreControl!.startAnimating()
        }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if(!self.isLoading && !self.loadDone && self.loadMoreControl != nil) {
            let lastOffset: CGFloat = scrollView.contentSize.height - scrollView.bounds.size.height
            if (scrollView.contentOffset.y >= lastOffset - 2.0 && scrollView.contentOffset.y <= lastOffset + 2.0) {
                self.loadMore()
            }else{
                self.loadMoreControl!.stopAnimating()
            }
        }
    }
    func loadMore(){
        self.isLoading = true
        if(!self.isShowSearch){
            APIFilmData.GetDataTable(self.className, count: self.pageCount, skip: self.skip, success: { (data : Array<PFObject>) -> Void in
                self.isLoading = false
                self.listGroup = self.listGroup + data
                self.skip = self.listGroup.count
                self.tblGroupFilm.reloadData()
                self.loadMoreControl.stopAnimating()
                if(data.count < self.pageCount){
                    self.loadDone = true
                    self.tblGroupFilm.tableFooterView = nil
                }
                }) { (erMessage) -> Void in
                    self.view.makeToast(erMessage, duration: 2, position: CSToastPositionCenter)
                    self.isLoading = false
            }
        }
        else{
            APIFilmData.SearchFilm(20, skip: self.skip, searchtext: self.searchText, success: { (data : Array<PFObject>) -> Void in
                self.isLoading = false
                self.listGroup = self.listGroup + data
                self.skip = self.listGroup.count
                self.tblGroupFilm.reloadData()
                self.loadMoreControl.stopAnimating()
                if(data.count < self.pageCount){
                    self.loadDone = true
                    self.tblGroupFilm.tableFooterView = nil
                }
            }) { (erMessage) -> Void in
                    self.view.makeToast(erMessage, duration: 2, position: CSToastPositionCenter)
                    self.isLoading = false
            }
        }
        
    }
    
}
