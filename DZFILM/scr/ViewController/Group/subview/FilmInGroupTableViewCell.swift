//
//  FilmInGroupTableViewCell.swift
//  DZFILM
//
//  Created by Nguyen Tien Dung on 9/23/15.
//  Copyright © 2015 Nguyen Tien Dung. All rights reserved.
//

import UIKit

class FilmInGroupTableViewCell: UITableViewCell {

    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var lblInfo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgPoster.layer.borderWidth = 0.5
        imgPoster.layer.borderColor = UIColor.lightGrayColor().CGColor
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
